<?php

namespace Drupal\smtp_oauth\Exception;

/**
 * General exception for errors thrown by PHPMailer.
 */
class PHPMailerException extends \Exception {

}
