<?php

namespace Drupal\smtp_oauth\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\Messenger;
use Symfony\Component\DependencyInjection\ContainerInterface;
// @see https://github.com/thephpleague/oauth2-google
use League\OAuth2\Client\Provider\Google;

/**
 * Implements the SMTP admin settings form.
 */
class OAuthSetupForm extends ConfigFormBase {

  /**
   * The D8 messenger.
   *
   * @var \Drupal\Core\Messenger\Messenger
   */
  protected $messenger;

  /**
   * Constructs $messenger and $config_factory objects.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Messenger\Messenger $messenger
   *   The D8 messenger object.
   */
  public function __construct(ConfigFactoryInterface $config_factory, Messenger $messenger) {
    $this->messenger = $messenger;
    parent::__construct($config_factory);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'smtp_oauth_setup';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->configFactory->getEditable('smtp_oauth.settings');

    $client_id = trim($config->get('oauth_client_id'));
    $client_secret = trim($config->get('oauth_client_secret'));

    // Redirect to Google to acquire Refresh
    // Borrowed heavily from PHPMailer's get_oauth_token.php
    $redirect_uri = (isset($_SERVER['HTTPS']) ? 'https://' : 'http://') . $_SERVER['HTTP_HOST'] . '/admin/config/system/smtp_oauth_setup';
    $params = [
      'clientId' => $client_id,
      'clientSecret' => $client_secret,
      'redirectUri' => $redirect_uri,
      'accessType' => 'offline'
    ];

    $provider = new Google($params);

    if (!empty($_GET['code'])) {
      // Refresh Token is passed back via a HTML parameter.
      // Try to get an access token (using the authorization code grant)
      //unset($_SESSION['provider']);
      $token = $provider->getAccessToken(
        'authorization_code',
        [
          'code' => $_GET['code']
        ]
      );
      // Use this to interact with an API on the users behalf
      // Use this to get a new access token if the old one expires
      $refresh_token = $token->getRefreshToken();
      if (!empty($refresh_token)) {
        $config->set('oauth_refresh_token', $token->getRefreshToken())->save();
      } else {
        \Drupal::messenger()->addError("The refresh token has returned blank.  It's possible it was sent once, then lost.  You cannot fix this by resetting the secret.  Delete the OAuth 2.0 Client ID and start over with a new ClientID and Secret.");
      }
    }

    $form['instructions'] = [
      '#type' => 'details',
      '#title' => $this->t('Google OAuth Instructions'),
      '#open' => TRUE,
    ];

    // Get top level domain.  (e.g. Convert "www.mysite.com" to "mysite.com")
    $domain = $_SERVER['HTTP_HOST'];
    $last_period_pos = strrpos($domain,'.');
    if ($last_period_pos !== FALSE) {
      $domain_substring = substr($domain,0,$last_period_pos);
      $second_last_period_pos = strrpos($domain_substring,'.');
      if ($second_last_period_pos !== FALSE) {
        $domain = substr($domain,$second_last_period_pos+1);
      }
    }

    if (empty($config->get('oauth_refresh_token'))) {
      $form['instructions'][]['#markup'] = '<p>To set up Google OAuth, follow these steps:</p>';
      $form['instructions'][]['#markup'] = '<ul><li>Log into the <a href="https://console.developers.google.com/projectselector2/apis/dashboard">Google Developer Console</a></li>';
      $form['instructions'][]['#markup'] = '<li>Click the "Select a project" dropdown, then click "New Project".  For Project Name, enter "SMTP OAuth" and click "Create"</li>';
      $form['instructions'][]['#markup'] = '<li>On the left, click "OAuth consent Screen".  Select User Type "External", then click "Create". For Application Name, enter "SMTP Auth".  For Authorized Domains, enter "' . $domain . '".  Click "Save". </li>';
      $form['instructions'][]['#markup'] = '<li>On the left, click "Credentials".  Click "+ Create Credentials", then click "OAuth client ID" from the pulldown. For Application Type, select "Web application". Set Name to "SMTP OAuth".  Under Authorized Redirect URIs, enter "' . $redirect_uri . '" , hit Enter, then click "Create". </li>';
      $form['instructions'][]['#markup'] = '<li>You will be provided with a Client ID and Client Secret. Copy these keys into the fields below, then click "Save Configuration".</li>';
      $form['instructions'][]['#markup'] = '<li>NOTE: If something goes wrong, it may be best to delete the ClientID entirely and start over.</li>';
    } else {
      $form['instructions'][]['#markup'] = '<p>You have a Client ID, a Client Secret, and a Refresh Token. You should be ready to send mail!</p>';
    }
    $form['oauth'] = [
      '#type' => 'details',
      '#title' => $this->t('Google OAuth'),
      '#description' => $this->t('Enter Google OAuth credentials here.'),
      '#open' => TRUE,
    ];
    $form['oauth']['oauth_client_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('OAuth Client ID'),
      '#size' => 70,
      '#default_value' => $config->get('oauth_client_id'),
      '#description' => $this->t('Google OAuth Client ID'),
      '#disabled' => $this->isOverridden('oauth_client_id'),
    ];
    $form['oauth']['oauth_client_secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('OAuth Client Secret'),
      '#size' => 30,
      '#default_value' => $config->get('oauth_client_secret'),
      '#description' => $this->t('Google OAuth Client Secret'),
      '#disabled' => $this->isOverridden('oauth_client_secret'),
    ];
    if (!empty($config->get('oauth_refresh_token'))) {
      $form['oauth']['oauth_refresh_token'] = [
        '#type' => 'textfield',
        '#title' => $this->t('OAuth Refresh Token'),
        '#size' => 90,
        '#default_value' => $config->get('oauth_refresh_token'),
        '#description' => $this->t('Google OAuth Refresh Token'),
        '#disabled' => $this->isOverridden('oauth_refresh_token'),
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * Check if config variable is overridden by the settings.php.
   *
   * @param string $name
   *   SMTP settings key.
   *
   * @return bool
   *   Boolean.
   */
  protected function isOverridden($name) {
    $original = $this->configFactory->getEditable('smtp_oauth.settings')->get($name);
    $current = $this->configFactory->get('smtp_oauth.settings')->get($name);
    return $original != $current;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $config = $this->configFactory->getEditable('smtp_oauth.settings');
    $mail_config = $this->configFactory->getEditable('system.mail');
    $mail_system = $mail_config->get('interface.default');

    // Updating config vars.
    $config_keys = [
      'oauth_client_id',
      'oauth_client_secret',
      'oauth_refresh_token',
    ];
    foreach ($config_keys as $name) {
      if (!$this->isOverridden($name)) {
        $config->set($name, trim($values[$name]))->save();
      }
    }

    if (!empty($values['oauth_client_id'])) {
      $client_id = trim($values['oauth_client_id']);
    }
    if (!empty($values['oauth_client_secret'])) {
      $client_secret = trim($values['oauth_client_secret']);
    }
    if (!empty($values['oauth_refresh_token'])) {
      $refresh_token = trim($values['oauth_refresh_token']);
    }

    if (!empty($client_id) && (!empty($client_secret) && empty($refresh_token))) {
      // Redirect to Google to acquire Refresh
      // Borrowed heavily from PHPMailer's get_oauth_token.php
      $redirect_uri = (isset($_SERVER['HTTPS']) ? 'https://' : 'http://') . $_SERVER['HTTP_HOST'] . '/admin/config/system/smtp_oauth_setup';
      $params = [
        'clientId' => $client_id,
        'clientSecret' => $client_secret,
        'redirectUri' => $redirect_uri,
        'accessType' => 'offline'
      ];

      $provider = new Google($params);
      $options = [
        'scope' => [
          'https://mail.google.com/'
        ]
      ];

      if (!isset($_GET['code'])) {
        // If we don't have an authorization code then get one
        $authUrl = $provider->getAuthorizationUrl($options);
        $_SESSION['oauth2state'] = $provider->getState();
        header('Location: ' . $authUrl);
        exit;
      }

    }
    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'smtp_oauth.settings',
    ];
  }

}
